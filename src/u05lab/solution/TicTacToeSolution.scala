package u05lab.solution

import scala.reflect.internal.util.TableDef.Column


/**
  * Created by margherita on 28/03/17.
  */
object TicTacToe extends App {
  sealed trait Player{
    def other: Player = this match {case X() => O(); case _ => X()}
    override def toString: String = this match {case X() => "X"; case _ => "O"}
  }
  case class X() extends Player
  case class O() extends Player

  case class Mark(x: Double, y: Double, player: Player)
  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Double, y: Double): Option[Player] = board.collectFirst{ case Mark(x1,y1,p) if (x==x1 && y==y1) => p}

  def placeAnyMark(board: Board, player: Player): Seq[Board] =  {var games = List[Mark]()
    if(!board.isEmpty) board.foreach(m => games = List(Mark(m.x, m.y,m.player)))
    for {x <- 0 to 2
         y <- 0 to 2
    } yield Mark(x,y,X()) :: games
  }

  def computeAnyGame(player: Player, moves: Int): Stream[Game] = moves match {
    case 0 => List(List(List[Mark]())).toStream
    case _ => for {g <- computeAnyGame(player.other, moves -1)
        i <- 0 to 2
        j <- 0 to 2
        mark = Mark(i, j, player)
        if (isOk(mark,g))
      } yield if (alreadyWon(g)) g else (mark :: g.head) :: g
  }

  def isOk(mark: Mark, game: Game): Boolean = {
    if (game.toStream.filter(b => !find(b, mark.x, mark.y).isEmpty).isEmpty) true else false
  }

  def alreadyWon(game: Game): Boolean = {
    for (i <- 0 to game.size -1) {
      val board: Board = game.apply(i)
      for (i <- 0 to 2) {
        if (trisRow(board,i) || trisColumn(board, i)) return true
      }
      return trisDiagonal(board)
    }
    false
  }

  def trisRow(board: Board, i: Int): Boolean = {
    try {
      if (find(board,i,0).get == find(board,i,1).get && find(board,i,1).get == find(board,i,2).get) return true
    } catch {
      case e: java.util.NoSuchElementException => false
    }
    false
  }

  def trisColumn(board: Board,i: Int): Boolean = {
    try {
      if (find(board,0,i).get == find(board,1,i).get && find(board,1,i).get == find(board,2, i).get) return true
    } catch {
      case e: java.util.NoSuchElementException => false
    }
    false
  }

  def trisDiagonal(board: Board): Boolean = {
    try {
      if ((find(board,0,0).get == find(board,1,1).get && find(board,1,1).get == find(board,2,2).get) ||
        (find(board,0,2).get == find(board,1,1).get && find(board,1,1).get == find(board,2,0).get)) return true
    } catch {
      case e: java.util.NoSuchElementException => false
    }
    false
  }

  def printBoards(game: Seq[Board]): Unit = {
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) {print(" ")
      if (board == game.head) println()}
    }
  }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0,0,X())),0,0)) // Some(X)
  println(find(List(Mark(0,0,X()),Mark(0,1,O()),Mark(0,2,X())),0,1)) // Some(O)
  println(find(List(Mark(0,0,X()),Mark(0,1,O()),Mark(0,2,X())),1,1)) // None

  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(),X()))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  printBoards(placeAnyMark(List(Mark(0,0,O())),X()))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...


  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  computeAnyGame(O(), 9) foreach {g => printBoards(g); println()}
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!

}
