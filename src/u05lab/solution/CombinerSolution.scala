package u05lab.solution

/**
  * Created by margherita on 28/03/17.
  */

trait Functions {
  def sum(a: List[Double]): Double
  def concat(a: Seq[String]): String
  def max(a: List[Int]): Int
}

trait Combiner[A] {
  def unit: A
  def combine(a: A, b: A): A
}

object FunctionsImpl extends Functions {

  private def combine[A](a: List[A], comb: Combiner[A]): A = a match {
    case h :: t => comb.combine(h,combine(t, comb))
    case _ => comb.unit
  }

  override def sum(a: List[Double]): Double = combine(a, new Combiner[Double] {
    override def combine(a: Double, b: Double) = a + b
    override def unit = 0.0
  })

  override def concat(a: Seq[String]): String = combine(a.toList, new Combiner[String] {
    override def combine(a: String, b: String) = a + b
    override def unit = ""
  })

  override def max(a: List[Int]): Int = combine(a, new Combiner[Int] {
      override def combine(a: Int, b: Int) = if (a > b) a else b
      override def unit = Int.MinValue
  })

}

object TryFunctions extends App {
  val f: Functions = FunctionsImpl
  println(f.sum(List(10.0,20.0,30.1))) // 60.1
  println(f.sum(List()))                // 0.0
  println(f.concat(Seq("a","b","c")))   // abc
  println(f.concat(Seq()))              // ""
  println(f.max(List(-10,3,-5,0)))      // 3
  println(f.max(List()))                // -2147483648
}
